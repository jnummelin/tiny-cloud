#!/usr/bin/env atf-sh

. $(atf_get_srcdir)/test_env.sh

export PREFIX="$srcdir"
export MOCK=echo
lib="$srcdir"/lib/tiny-cloud/init
PROVIDERS="aws azure gcp nocloud oci"

init_tests \
	userdata_type \
	run_userdata


userdata_type_body() {
	mkdir -p var/lib/cloud
	for c in $PROVIDERS; do
		rm -f var/lib/cloud/user-data
		CLOUD="$c" atf_check \
			-o match:"missing" \
			sh -c ". \"$lib\"; userdata_type"

		echo "#tiny-cloud-config" > var/lib/cloud/user-data
		CLOUD="$c" atf_check \
			-o match:"tiny-cloud-config" \
			sh -c ". \"$lib\"; userdata_type"

		echo "no-content-type" > var/lib/cloud/user-data
		CLOUD="$c" atf_check \
			-o match:"unknown" \
			sh -c ". \"$lib\"; userdata_type"

		echo "#!/bin/sh" > var/lib/cloud/user-data
		CLOUD="$c" atf_check -s exit:0 \
			-o match:"script" \
			sh -c ". \"$lib\"; userdata_type"
	done
}

run_userdata_body() {
	fake_userdata_nocloud <<-EOF
		#!/bin/sh
		echo "hello from user-data"
	EOF
	CLOUD="nocloud" atf_check \
		sh -c ". \"$lib\"; init__save_userdata"
	CLOUD="nocloud" atf_check \
		-o match:"hello from user-data" \
		sh -c ". \"$lib\"; init__run_userdata"
	grep "hello from user-data" var/log/user-data.log || atf_fail "user-data.log failed"
	grep -w "0" var/log/user-data.exit || atf_fail "user-data.exit failed"
}
