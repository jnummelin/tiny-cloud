#!/usr/bin/env atf-sh

. $(atf_get_srcdir)/test_env.sh

export PREFIX="$srcdir"
export MOCK=echo
PROVIDERS="alpine aws azure gcp nocloud oci"

init_tests \
	tiny_cloud_help \
	no_metadata_early \
	no_userdata_net \
	no_userdata_main \
	no_userdata_final

tiny_cloud_help_body() {
	for provider in $PROVIDERS; do
		CLOUD="$provider" atf_check -s not-exit:0 \
			-e match:"Usage" \
			tiny-cloud --invalid

		CLOUD="$provider" atf_check -s exit:0 \
			-o match:"Usage" \
			tiny-cloud --help
	done
}

no_metadata_early_body() {
	fake_netcat
	for provider in $PROVIDERS; do
		CLOUD="$provider" atf_check \
			-e not-match:"UNKNOWN" \
			-e not-match:"not found" \
			-e not-match:"o such file" \
			-o match:"rc-update add.*sshd" \
			tiny-cloud early
	done
}

no_userdata_net_body() {
	fake_netcat
	for provider in $PROVIDERS; do
		CLOUD="$provider" atf_check \
			-e not-match:"UNKNOWN" \
			-e match:"save_userdata.*DONE" \
			tiny-cloud net
	done
}

no_userdata_main_body() {
	fake_netcat
	for provider in $PROVIDERS; do
		# we should not set empty hostname
		# we should not create .ssh dir for non-existing user
		CLOUD="$provider" atf_check \
			-e not-match:"UNKNOWN" \
			-o not-match:"hostname.*-F" \
			-o not-match:"chown.*/\.ssh" \
			tiny-cloud main
		for i in etc/hostname .ssh; do
			if [ -e "$i" ]; then
				atf_fail "$i should not have been created"
			fi
		done
	done
}

no_userdata_final_body() {
	fake_netcat
	for provider in $PROVIDERS; do
		CLOUD="$provider" atf_check \
			-e not-match:"UNKNOWN" \
			-e match:"bootstrap_complete .*" \
			tiny-cloud final
		CLOUD="$provider" atf_check \
			-e match:"bootstrap marked.*incomplete" \
			tiny-cloud --bootstrap incomplete
	done
}
